import subprocess
import sys, fileinput
import json
import os
from subprocess import Popen, PIPE

testJustScript = False

# structureRequestAgi = {"taskSelected":{"PHOTO_ALIGN":True},"quality":"high"}
tasksForAgisoftDefault = {
    "taskSelected":{
        'MESH': False, 
        'DENSE_CLOUD': False, 
        'TEXTURE': False, 
        'PHOTO_ALIGN': True, 
        'POLY_DECIMATION': False
    },
    "quality":"low",
    "directoryImages":"/Users/zarfaouik/Desktop/Electron/AGISOFT_TEST/monument"
}
tasksForAgisoft = '{}'

for line in fileinput.input():
    tasksForAgisoft = line
    pass

pathProgram = 'D:/Agisoft/photoscan.exe'
pathProgram = '/home/karim/Scrivania/photoscan-pro/photoscan.sh'

print('LAUNCH AGISOFT FILE - SCRIPT START')
sys.stdout.flush()

currentDir = os.path.dirname(os.path.abspath(__file__))
pathScript = currentDir + '/agisoftScript.py'

tasksForAgisoftDefault = json.dumps(tasksForAgisoftDefault)
print(tasksForAgisoft)
args = [pathProgram, '-r', pathScript, tasksForAgisoft]
#subprocess.call(args)
try:
    p = Popen(args, stdin=PIPE, stdout=PIPE, stderr=PIPE, shell=False)
    print("pid_" + str(p.pid))
    while True:
        line = p.stdout.readline()
        if not line: break
        line = line.decode("utf-8")
        if line.strip():
            print(line)
            sys.stdout.flush()
except subprocess.CalledProcessError as e:
    print("Errore")
    print(e)
#output, err = p.communicate(b"input data that is passed to subprocess' stdin")
#rc = p.returncode


print('FINE 1 - START')
sys.stdout.flush()

print('Launcher script agisoft ended.')