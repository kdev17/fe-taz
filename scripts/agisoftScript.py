import os
import PhotoScan
import json
import sys
import time
import pickle


print('SCRIPT 1 - START')
# NON RIMUOVERE LA PRINT QUA SOTTO
print('pid_' + str(os.getpid()))

mapOfOperation = {'MESH': True, 'DENSE_CLOUD': True, 'TEXTURE': True, 'PHOTO_ALIGN': True, 'POLY_DECIMATION': False}
requestAgisoft=json.loads(sys.argv[1]) 

# CONSTANT
CURRENT_OPERATION = ""
percentDecimateModel = 15 / 100 # Decimate model percentual
keypoint_limit_val=40000
tiepoint_limit_val=4000 

PhotoAlignmentQ = PhotoScan.LowAccuracy #Photo alignment | Quality low --> PhotoScan.HighAccuracy
BuildDepthMapsQ = PhotoScan.LowestQuality #Dense cloud | Quality low --> PhotoScan.HighQuality
MeshFaceCountQuality = PhotoScan.FaceCount.LowFaceCount #Mesh Quality --> PhotoScan.FaceCount.MediumFaceCount

print(requestAgisoft)

# ------------- INPUT PARAMETETRI -------------
taskSelected = requestAgisoft['taskSelected']
mapOfOperation = taskSelected

if(mapOfOperation == {}):
    print('MapOp: ',mapOfOperation)
    sys.exit()

quality = requestAgisoft['quality']
print(quality)

path_photos = requestAgisoft['directoryImages']
print(path_photos)

if 'key_point' in requestAgisoft:
    keypoint_limit_val = requestAgisoft['key_point']

if 'tie_point' in requestAgisoft:
    tiepoint_limit_val = requestAgisoft['tie_point']

if 'decimate_model_perc' in requestAgisoft:
    percentDecimateModel = requestAgisoft['decimate_model_perc']

filenameProject = 'projectAgisoft-1'
pathFileProject = path_photos + "/" + filenameProject + ".psx"
print(filenameProject)

if quality == 'high': 
    PhotoAlignmentQ = PhotoScan.HighestAccuracy #Photo alignment Quality 
    BuildDepthMapsQ = PhotoScan.UltraQuality #Dense cloud Quality
    MeshFaceCountQuality = PhotoScan.FaceCount.HighFaceCount #Mesh Quality 

def saveObj(fileName, obj):
    print('Salvo op effettuate')
    print(path_photos + "/" + fileName)
    with open(path_photos + "/" + fileName, 'wb') as _file:
      pickle.dump(obj, _file)

def loadObj(fileName):
    print('carico f')
    path = path_photos + "/" + fileName
    if os.path.exists(path):
        with open(path, 'rb') as _file:
            obj = pickle.load(_file)
            print(obj)
            return obj
    else:
        return None

def getEsito(currentOperation, msg, code):
        esito = {"currentOperation": currentOperation ,"msgEsito": msg, "codeEsito": code}
        return json.dumps(esito)

def progress_print(p):
        print('Current task name:' + CURRENT_OPERATION)
        print('Current task progress: {:.2f}%'.format(p))
        sys.stdout.flush()

def progress_print_json(p):
        obj = {"taskName": CURRENT_OPERATION, "taskProgress": p}
        print(json.dumps(obj))
        sys.stdout.flush()

def align(chunk):
        # Processing:
        CURRENT_OPERATION = "Photo alignment"
        chunk.matchPhotos(accuracy=PhotoScan.LowAccuracy, generic_preselection=True, reference_preselection=False,
                          tiepoint_limit=tiepoint_limit_val, keypoint_limit=keypoint_limit_val, progress=progress_print_json)
        chunk.alignCameras()

        CURRENT_OPERATION = "Build depth maps"
        chunk.buildDepthMaps(quality=PhotoScan.LowestQuality,
                            filter=PhotoScan.AggressiveFiltering, progress=progress_print_json)

def getChunkByLabel(doc, label):
        for tmpChunk in doc.chunks:
            print(tmpChunk.label)
            if tmpChunk.label == label:
                return tmpChunk

def getListPhotoDir(pathDir):
        image_list = os.listdir(pathDir)
        photo_list = list()

        for photo in image_list:
            if photo.rsplit(".", 1)[1].lower() in ["jpg", "jpeg", "tif", "tiff"]:
                photo_list.append("/".join([pathDir, photo]))
        
        return photo_list

def lockFile(pathDir):
        if os.path.exists( pathDir + '/test1.files/lock' ):
            os.remove( pathDir + '/test1.files/lock' )

def taskIsEnable(task, mapTasks):
    return mapTasks and (task in mapTasks) and mapTasks[task]

def printPhaseName(phaseName):
    print('-------------- ' + phaseName + ' -------------------')


try:
    doc = PhotoScan.app.document
    pastMapOfOperation = loadObj('pastMapOfOperation.txt') 
    chunk = None
    isNewProject = True

    if os.path.isfile(pathFileProject): #WORKAROUND TROVATO SU GOOGLE SE NO DA --> Document.open(): The document is opened in read-only mode because it is already in use.
        doc = PhotoScan.app.document
        doc.open(pathFileProject, read_only=True)
        doc.read_only = False
        doc = PhotoScan.app.document
        doc.open(pathFileProject, read_only=False)
        isNewProject = False
        chunk = doc.chunk
    else:
        chunk = doc.addChunk()
        chunk.label = "MyChunk"

    print('isNewProject: ' + str(isNewProject))
    print('Path: ' + pathFileProject)
         

    if((taskIsEnable('PHOTO_ALIGN', mapOfOperation) and not taskIsEnable('PHOTO_ALIGN', pastMapOfOperation)) or 
        (taskIsEnable('PHOTO_ALIGN', mapOfOperation) and isNewProject)): 
        # AGGIUNGO LE FOTO AL CHUNK
        list_photo = getListPhotoDir(path_photos)
        chunk.addPhotos(list_photo)

        #PHOTO ALIGNMENT
        CURRENT_OPERATION = "Photo alignment"
        printPhaseName(CURRENT_OPERATION)
        chunk.matchPhotos(accuracy=PhotoAlignmentQ, generic_preselection=True, reference_preselection=False,
                          tiepoint_limit=tiepoint_limit_val, keypoint_limit=keypoint_limit_val, progress=progress_print_json)
        chunk.alignCameras()
    
    print(taskIsEnable('DENSE_CLOUD', pastMapOfOperation))
    #DENSE CLOUD
    if ((taskIsEnable('DENSE_CLOUD', mapOfOperation) and not taskIsEnable('DENSE_CLOUD', pastMapOfOperation)) or 
        (taskIsEnable('DENSE_CLOUD', mapOfOperation) and isNewProject)): 
        CURRENT_OPERATION = "Dense cloud"
        printPhaseName(CURRENT_OPERATION)
        chunk.buildDepthMaps(quality=BuildDepthMapsQ,filter=PhotoScan.AggressiveFiltering, progress=progress_print_json)
        a = chunk.buildDenseCloud(progress=progress_print_json)
        print(a)

    # MESH --> recupera facce
    if ((taskIsEnable('MESH', mapOfOperation) and not taskIsEnable('MESH', pastMapOfOperation)) or 
        (taskIsEnable('MESH', mapOfOperation) and isNewProject)):
        CURRENT_OPERATION = "Mesh"
        printPhaseName(CURRENT_OPERATION)
        a = chunk.buildModel(surface=PhotoScan.Arbitrary, interpolation=PhotoScan.EnabledInterpolation,face_count=MeshFaceCountQuality, progress=progress_print_json)

    # DECIMATE MESH
    if ((taskIsEnable('POLY_DECIMATION', mapOfOperation) and not taskIsEnable('POLY_DECIMATION', pastMapOfOperation)) or
        (taskIsEnable('POLY_DECIMATION', mapOfOperation) and isNewProject)):
        CURRENT_OPERATION = "Decimate model"
        printPhaseName(CURRENT_OPERATION)
        statistics = chunk.model.statistics()
        #Rimuovo un 15%
        decimate_face = statistics.faces - (statistics.faces * percentDecimateModel)
        decimate_face = round(decimate_face)
        print(statistics.faces, ' ', decimate_face)
        sys.stdout.flush()
        tmp = chunk.decimateModel(face_count=decimate_face, progress=progress_print_json)
        statistics = chunk.model.statistics()
        print(statistics.faces, ' ', decimate_face)
        sys.stdout.flush()

    # TEXTURE
    if ((taskIsEnable('TEXTURE', mapOfOperation) and not taskIsEnable('TEXTURE', pastMapOfOperation)) or
        (taskIsEnable('TEXTURE', mapOfOperation) and isNewProject)):
        CURRENT_OPERATION = "Texture"
        printPhaseName(CURRENT_OPERATION)
        chunk.buildUV(mapping=PhotoScan.GenericMapping)
        chunk.buildTexture(blending=PhotoScan.MosaicBlending, size=4096, progress=progress_print_json)


    CURRENT_OPERATION = "Salvataggio"
    printPhaseName(CURRENT_OPERATION)
    
    if isNewProject:
       print("Salvo il progetto passato")
       doc.save(pathFileProject)
    else:
       print("Salvo il nuovo progetto")
       doc.save()

    saveObj('pastMapOfOperation.txt', mapOfOperation) 

    print(getEsito(CURRENT_OPERATION, pathFileProject, 0))

     # /Applications/PhotoScanPro.app/Contents/MacOS/PhotoScanPro -r /Users/zarfaouik/Desktop/Electron/angular-electron/scripts/script1.py
except Exception as e:
    print(e)
    print('current op: ', CURRENT_OPERATION)
    print(getEsito(CURRENT_OPERATION, "Errore", -1))
    
