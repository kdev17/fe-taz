export const AppConfig = {
  production: true,
  environment: 'PROD',
  // baseUrl: 'http://169.254.221.185:8080/',
  baseUrl: 'localhost:8080',
  scriptDirectory: 'D:/Desktop/tazzari/frontend/scripts/launchAgisoft.py',
  karimMode: true
};
