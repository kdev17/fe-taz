import subprocess
import sys, fileinput
import json
import os

testJustScript = False

# structureRequestAgi = {"taskSelected":{"PHOTO_ALIGN":True},"quality":"high"}
tasksForAgisoftDefault = {
    "taskSelected":{
        'MESH': False, 
        'DENSE_CLOUD': False, 
        'TEXTURE': False, 
        'PHOTO_ALIGN': True, 
        'POLY_DECIMATION': False
    },
    "quality":"low",
    "directoryImages":"/Users/zarfaouik/Desktop/Electron/AGISOFT_TEST/monument"
}
tasksForAgisoft = '{}'

for line in fileinput.input():
    tasksForAgisoft = line
    pass

pathProgram = '/Applications/PhotoScanPro.app/Contents/MacOS/PhotoScanPro'

currentDir = os.path.dirname(os.path.abspath(__file__))
pathScript = currentDir + '/agisoftScript.py'

tasksForAgisoftDefault = json.dumps(tasksForAgisoftDefault)
args = [pathProgram, '-r', pathScript, tasksForAgisoft]
subprocess.call(args)

print('Launcher script agisoft ended.')