import { Component, OnInit, Input } from '@angular/core';
import { FileManagerService } from '../../providers/file-manager.service';

@Component({
  selector: 'app-modal-agisoft-input',
  templateUrl: './modal-agisoft-input.component.html',
  styleUrls: ['./modal-agisoft-input.component.scss']
})
export class ModalAgisoftInputComponent implements OnInit {

  percentDecimateModel;
  saveAs;
  @Input() quality;
  path_photos;

  constructor(public fsManager: FileManagerService) { }

  ngOnInit() {
  }

}
