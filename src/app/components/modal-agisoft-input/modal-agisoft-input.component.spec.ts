import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAgisoftInputComponent } from './modal-agisoft-input.component';

describe('ModalAgisoftInputComponent', () => {
  let component: ModalAgisoftInputComponent;
  let fixture: ComponentFixture<ModalAgisoftInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAgisoftInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAgisoftInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
