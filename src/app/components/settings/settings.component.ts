import { Component, OnInit, OnDestroy, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RadiusDataService } from '../../providers/save-data.service';
import { CamComunicationService } from '../../providers/cam-comunication.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit, OnDestroy {
  radiusForm: FormGroup;
  showRadius = false;
  minimize = true;
  subs = [];
  @Output() checkPiConnection = new EventEmitter();


  constructor(private fb: FormBuilder, private saveData: RadiusDataService, private camService: CamComunicationService) {
    this.inizializzaForm();
  }

  inizializzaForm() {
    this.radiusForm = this.fb.group({
      radius1_6: [this.saveData.radiusPlane ? this.saveData.radiusPlane.radius1_6 : ''],
      radius3_2: [this.saveData.radiusPlane ? this.saveData.radiusPlane.radius3_2 : ''],
    });
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subs.map(el => el.unsubscribe);
  }

  saveRadius() {
    this.saveData.saveRadius(this.radiusForm.value);
    this.toggleRadius();
  }

  toggleRadius() {
    this.showRadius = !this.showRadius;
    this.minimize = !this.showRadius ? true : false;
  }

  toggleMinimize() {
    this.showRadius = false;
  }

  restart() {
    this.camService.restart();
  }

  checkPiHandleEvent($event: MatSlideToggleChange) {
    this.checkPiConnection.next($event.checked);
  }

}
