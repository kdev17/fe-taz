import { Component, OnInit, OnDestroy } from '@angular/core';
import { CamComunicationService } from '../../providers/cam-comunication.service';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/debounceTime';
import { AlertNotificationService } from '../../providers/alert-notification.service';
import { AppConfig } from '../../../environments/environment';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {

  piConnectionOk = true;
  observable = [];
  constructor(private camService: CamComunicationService, private notification: AlertNotificationService) {
    this.camService.checkPiConnection = true;
   }

  ngOnInit() {
    this.polling();
  }

  ngOnDestroy() {
    this.observable.map(el => el.unsubscribe());
  }

  piIsConnected() {
    if (this.camService.checkPiConnection) {
      // console.log('Check connessione');
      this.camService.piIsConnected()
        .then(result => {
          this.piConnectionOk = result.esito === 'done';
          this.notification.hideAlert();
        }).catch(err => {
          this.piConnectionOk = false;
        });
    }
  }

  polling() {
    IntervalObservable.create(5000)
      .debounceTime(300)
      .subscribe(() => {
        this.piIsConnected();
      });
  }

  activePiConnection($event) {
    
    this.camService.checkPiConnection = $event;
    if(!this.camService.checkPiConnection) { 
      this.piConnectionOk = true;
      this.notification.hideAlert();
    }
  }
}
