import { Component, OnInit, Input, Output, EventEmitter, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-image-preview',
  templateUrl: './image-preview.component.html',
  styleUrls: ['./image-preview.component.scss']
})
export class ImagePreviewComponent implements OnInit, AfterViewInit {

  @Input() pathImg;
  @Output() closeImageEmitter = new EventEmitter<any>();
  rotationDeg = 0;

  constructor() { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    // console.log("old path: " + this.pathImg)
    this.pathImg = this.pathImg.replace(/\\/g,"/")
    // console.log("new path: "+ this.pathImg)
    document.getElementById('image').style.backgroundImage = 'url(' + this.pathImg + ')';
  }

  rotate(deg?) {
    const img = document.getElementById('container');
    let degRotation = (deg!=undefined && deg!=null)  ? deg : 90;

    if(degRotation < 0 && this.rotationDeg === 0) {
      degRotation = 360 - Math.abs(degRotation);
    }
    this.rotationDeg = (this.rotationDeg + degRotation) % 360;
    img.className = "rotate" + this.rotationDeg;

  }

  closeImage() {
    this.rotationDeg = 0;
    this.closeImageEmitter.next('Chiudi anteprima image');
  }
}
