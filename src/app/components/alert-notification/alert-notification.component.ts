import { Component, OnInit } from '@angular/core';
import { AlertNotificationService, DONE_CODE, WARN_CODE, ERROR_CODE } from '../../providers/alert-notification.service';

@Component({
  selector: 'app-alert-notification',
  templateUrl: './alert-notification.component.html',
  styleUrls: ['./alert-notification.component.scss']
})
export class AlertNotificationComponent implements OnInit {

  constructor(public alertService: AlertNotificationService) { }

  ngOnInit() {
  }

  isWarning() {
    return this.alertService.typeAlert === WARN_CODE;
  }

  isError() {
    return this.alertService.typeAlert === ERROR_CODE;
  }

  isDone() {
    return this.alertService.typeAlert === DONE_CODE;
  }

  getMessage() {
    return this.alertService.messaggio;
  }

  close() {
    this.alertService.hideAlertForce();
  }

}
