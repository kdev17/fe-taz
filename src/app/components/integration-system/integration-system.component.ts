import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CamComunicationService } from '../../providers/cam-comunication.service';
import { PhotoSettingService } from '../../providers/photo-setting.service';
import { MoveResponse } from '../../interfaces/move';
import { AlertNotificationService } from '../../providers/alert-notification.service';
import { FileManagerService } from '../../providers/file-manager.service';
import { remote, app, EventEmitter } from 'electron';
import { PythonShell } from 'python-shell';
import { AgisoftComService } from '../../providers/agisoft-com.service';
import { SelectionModel } from '@angular/cdk/collections';
import { MatListOption, MatSelectionListChange } from '@angular/material/list';

export const AGISOFT_OPERATION = ['Photo Alignment', 'Dense cloud', 'Mesh', 'Texture', 'Poly Decimation'];

@Component({
  selector: 'app-integration-system',
  templateUrl: './integration-system.component.html',
  styleUrls: ['./integration-system.component.scss']
})
export class IntegrationSystemComponent implements OnInit {
  planDimensionSettingsForm: FormGroup;
  planDimValues = [1.60, 3.20];
  qualityValues = ['Low', 'High', 'Ultra high'];
  shotsMode = 'auto';

  // AGISOFT
  operationAgiSelected = '';
  modeAgisoft;
  selectedOptions = [];
  pathImagesForAgisoft = '';

  constructor(private fb: FormBuilder, public settingPhService: PhotoSettingService,
    private camService: CamComunicationService, private alertNotication: AlertNotificationService,
    private fileManger: FileManagerService, private agisoftService: AgisoftComService) {
    this.planDimensionSettingsForm = fb.group({
      plane: ['', Validators.required],
      quality: ['', Validators.required],
      direction: ['', Validators.required]
    });
  }

  ngOnInit() {
  }

  isAuto() {
    return this.shotsMode === 'auto';
  }
  reset() {
    this.camService.reset({});
  }

  private getValuePlaneDimesionField() {
    if (this.planDimensionSettingsForm.get('plane').value) {
      this.alertNotication.hideAlert();
      return this.planDimensionSettingsForm.get('plane').value;
    } else {
      this.alertNotication.warnMsg('Insert the plane dimension.');
    }
    return false;
  }
  private getValueFormField(quality) {
    if (this.planDimensionSettingsForm.get(quality).value) {
      this.alertNotication.hideAlert();
      return this.planDimensionSettingsForm.get(quality).value;
    } else {
      this.alertNotication.warnMsg('Insert the ' + quality);
    }
    return false;
  }

  moveRight(continuosM?) {
    const quality = this.getValueFormField('quality');
    const planeDim = this.getValuePlaneDimesionField();
    if (planeDim && quality) {
      this.camService.rightMove(planeDim, quality, continuosM)
      .then((result: MoveResponse) => {
          if (result && result.esito) {
            this.camService.pollingFunction(this.camService.checkMovement, this.camService);
          }
        });
    }
  }

  moveLeft(continuosM?) {
    const quality = this.getValueFormField('quality');
    const planeDim = this.getValuePlaneDimesionField();
    if (planeDim && quality) {
      this.camService.leftMove(planeDim, quality, continuosM)
        .then((result: MoveResponse) => {
          if (result && result.esito) {
            this.camService.pollingFunction(this.camService.checkMovement, this.camService);
          }
        });
    }
  }

  singleShot() {
    if (this.settingPhService.formIsValid()) {
      this.camService.shotSingle(this.settingPhService.photoSettingsForm.value).then(data => {
        if (data) {
        }
      });
    }
  }

  stopShot() {
    this.camService.stopShot();
  }

  autoShot() {
    // fixme fare la stessa cosa anche con il form rigurdante il plane
    if (this.settingPhService.formIsValid() && this.planDimensionSettingsForm.valid) {
      const dataAutoShoot = Object.assign(this.planDimensionSettingsForm.value, this.settingPhService.photoSettingsForm.value);
      this.camService.autoShootMode(dataAutoShoot);
    } else if (this.planDimensionSettingsForm.invalid) {
      this.alertNotication.warnMsg('Fill out the form with the plane dimension, the direction and the quality.');
      // this.settingPhService.msgAlert = 'Riempire i campi plane dimension e quality.';
    }
  }

  // AGISOFT OPERATION

  setAgisoftMode(modeAgisoft) {
    this.modeAgisoft = modeAgisoft;
  }

  startSpeedUp(quality) {
    this.agisoftService.speedUpMode(quality, this.pathImagesForAgisoft);
  }

  startStepBySteep(quality) {
    this.agisoftService.stepByStepMode(this.selectedOptions, quality, this.pathImagesForAgisoft);
  }

  getAgisoftOp() {
    return this.agisoftService.getAgisoftOp();
  }

  chooseDirectoryForAgisoftTask() {
    const tmp = this.fileManger.openDirectoryReturnPath();
    if (tmp) {
      this.pathImagesForAgisoft = tmp[0];
    }
  }

  waitAgisoftResponse() {
    return this.agisoftService.disableBtnAgisoft;
  }
}
