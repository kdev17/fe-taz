import { Component, OnInit } from '@angular/core';
import { SettingsAgisoftService } from '../../../../providers/settings-agisoft.service';

@Component({
  selector: 'app-settings-agisoft',
  templateUrl: './settings-agisoft.component.html',
  styleUrls: ['./settings-agisoft.component.scss']
})
export class SettingsAgisoftComponent implements OnInit {

  constructor(public settingsService: SettingsAgisoftService) { }

  ngOnInit() {
  }

}
