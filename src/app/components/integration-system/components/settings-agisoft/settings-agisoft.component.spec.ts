import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsAgisoftComponent } from './settings-agisoft.component';

describe('SettingsAgisoftComponent', () => {
  let component: SettingsAgisoftComponent;
  let fixture: ComponentFixture<SettingsAgisoftComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsAgisoftComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsAgisoftComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
