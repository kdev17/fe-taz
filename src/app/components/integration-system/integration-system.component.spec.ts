import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntegrationSystemComponent } from './integration-system.component';

describe('IntegrationSystemComponent', () => {
  let component: IntegrationSystemComponent;
  let fixture: ComponentFixture<IntegrationSystemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntegrationSystemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntegrationSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
