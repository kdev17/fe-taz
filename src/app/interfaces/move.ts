import { Response } from './response';

export interface MoveRequest {
    plane: String;
}

export interface MoveResponse extends Response {
    info?: String;
}

export interface ResetRequest {
    plane: String;
}

export interface ResetResponse extends Response {
    info?: String;
}
