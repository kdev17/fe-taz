import { Response } from './response';
import { ImageResponse } from './preview';

// TODO: controllare websocket su frontend, chiedere numero foto per piano-qualità
export interface AutoShotRequest {
    iso?: String;
    whitebalance?: String;
    diafram?: String;
    shutterSpeed?: String;
    plane?: String;
    quality?: String;
    radius?: any;
}

export interface AutoShotResponse extends Response {
    port?: Number;
    completamento?: boolean;
    esito?: string;
    perc?: number;
    photos?: Array<ImageResponse>;
}

export interface StopShotResponse extends Response {
    info?: String;
}
