import { Response } from './response';

export interface PreviewRequest {
    iso?: String;
    whiteBalance?: String;
    diafram?: String;
    shutterSpeed?: String;
}

export interface PreviewResponse extends Response {
    photos?: Array<ImageResponse>;
}

export interface ImageResponse {
    name?: String;
    data?: String;
}



