import 'zone.js/dist/zone-mix';
import 'reflect-metadata';
import '../polyfills';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule, HttpClient } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { ElectronService } from './providers/electron.service';

import { WebviewDirective } from './directives/webview.directive';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';
import { MatTabsModule } from '@angular/material/tabs';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import {MatRadioModule} from '@angular/material/radio';
import {MatListModule} from '@angular/material/list';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatInputModule} from '@angular/material/input';

import { ISO_VALUES, SHUTTER_SPEED_VALUES, DIAPHRAGM_VALUES, WHITE_BALANCE_VALUES } from './const/settings-photo';
import { IntegrationSystemComponent } from './components/integration-system/integration-system.component';
import { HeaderComponent } from './components/header/header.component';
import { HttpService } from './providers/http-service.service';
import { CamComunicationService } from './providers/cam-comunication.service';
import { FileManagerService } from './providers/file-manager.service';
import { AlertNotificationService } from './providers/alert-notification.service';
import { PhotoSettingService } from './providers/photo-setting.service';
import { LightboxModule } from 'angular2-lightbox';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { AlertNotificationComponent } from './components/alert-notification/alert-notification.component';
import { SpinnerLoadingService } from './providers/spinner-loading.service';
import { ModalAgisoftInputComponent } from './components/modal-agisoft-input/modal-agisoft-input.component';
import { SettingsComponent } from './components/settings/settings.component';
import { ReversePipe } from './customPipe/reverse.pipe';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { SettingsAgisoftComponent } from './components/integration-system/components/settings-agisoft/settings-agisoft.component';
import { UtilsService } from './providers/utils.service';
import { ImagePreviewComponent } from './components/image-preview/image-preview.component';

// AoT requires an exported function for factories
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    WebviewDirective,
    DashboardComponent,
    IntegrationSystemComponent,
    HeaderComponent,
    AlertNotificationComponent,
    ModalAgisoftInputComponent,
    SettingsComponent,
    ReversePipe,
    SettingsAgisoftComponent,
    ImagePreviewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    BrowserAnimationsModule,
    BrowserModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    MatTabsModule,
    MatButtonToggleModule,
    LightboxModule,
    MatTooltipModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatListModule,
    MatCheckboxModule,
    MatProgressBarModule,
    MatInputModule,
    MatSlideToggleModule
  ],
  providers: [
    ElectronService,
    HttpService,
    CamComunicationService,
    FileManagerService,
    PhotoSettingService,
    AlertNotificationService,
    SpinnerLoadingService,
    UtilsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
