import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ISO_VALUES, WHITE_BALANCE_VALUES, DIAPHRAGM_VALUES, SHUTTER_SPEED_VALUES } from '../../const/settings-photo';
import { HttpService } from '../../providers/http-service.service';
import { CamComunicationService } from '../../providers/cam-comunication.service';
import { FileManagerService } from '../../providers/file-manager.service';
import { PhotoSettingService } from '../../providers/photo-setting.service';
import { AlertNotificationService } from '../../providers/alert-notification.service';
import { BrowserWindow } from 'electron';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  // photoSettingsForm: FormGroup;
  destinationImages;
  imagesPreview;
  msgAlert;
  imagePreviewPath;

  constructor(public settingPhService: PhotoSettingService, private fb: FormBuilder,
    private camService: CamComunicationService, public fsManager: FileManagerService,
    private alertNotification: AlertNotificationService) {
  }

  ngOnInit() {
  }

  showAttributes() {
    // console.log(this);
  }
  getValuesSpec(key) {
    switch (key) {
      case 'ISO_VALUES':
        return ISO_VALUES;
      case 'WHITE_BALANCE_VALUES':
        return WHITE_BALANCE_VALUES;
      case 'DIAPHRAGM_VALUES':
        return DIAPHRAGM_VALUES;
      case 'SHUTTER_SPEED_VALUES':
        return SHUTTER_SPEED_VALUES;
      default:
        break;
    }
  }

  getPreview() {
    if (this.settingPhService.formIsValid()) {
      this.camService.getPreview(this.settingPhService.photoSettingsForm.value).then(data => {
        if (data) {
          /* this.imagesFromPreviewFolder(); */
        }
      });
    }
  }

  singleShot() {
    if (this.settingPhService.formIsValid()) {
      this.camService.shotSingle(this.settingPhService.photoSettingsForm.value).then(data => {
        if (data) {
        }
      });
    }
  }

  cleanPreview() {
    this.camService.cleanPreview();
  }

  chooseDirectory() {
    const tmp = this.fsManager.openDirectoryReturnPath();
    if (tmp) {
      this.fsManager.folderSelected = tmp[0];
      this.alertNotification.hideAlert();
    }
  }

  showImage(path) {
    this.imagePreviewPath = path;
    //window.open(path, '_blank', 'nodeIntegration=no');
  }

  closePreview($event) {
    console.log($event);
    this.imagePreviewPath = null;
  }
}
