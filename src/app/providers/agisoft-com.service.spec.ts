import { TestBed, inject } from '@angular/core/testing';

import { AgisoftComService } from './agisoft-com.service';

describe('AgisoftComService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AgisoftComService]
    });
  });

  it('should be created', inject([AgisoftComService], (service: AgisoftComService) => {
    expect(service).toBeTruthy();
  }));
});
