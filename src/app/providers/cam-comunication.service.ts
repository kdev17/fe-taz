import { Injectable, OnDestroy } from '@angular/core';
import { Http2Server } from 'http2';
import { HttpService } from './http-service.service';
import { PreviewRequest, PreviewResponse } from '../interfaces/preview';
import { AppConfig } from '../../environments/environment.prod';
import * as fs from 'fs';
import * as https from 'https';
import * as FileSaver from 'file-saver';
import { MoveRequest, MoveResponse, ResetRequest, ResetResponse } from '../interfaces/move';
import { StopShotResponse, AutoShotRequest, AutoShotResponse } from '../interfaces/shot';
import { FileManagerService, PREVIEW_SHOOT_DIR, SINGLE_SHOOT_DIR, AUTO_SHOOT_DIR } from './file-manager.service';
import { IntervalObservable } from 'rxjs/observable/IntervalObservable';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/debounceTime';
import { SpinnerLoadingService } from './spinner-loading.service';
import { AgisoftComService } from './agisoft-com.service';
import { RadiusDataService } from './save-data.service';
import * as _ from 'lodash';
import 'rxjs/add/operator/map';
import { Subject, Subscription, Observable } from 'rxjs';
import { UtilsService } from './utils.service';

@Injectable({
  providedIn: 'root'
})
export class CamComunicationService implements OnDestroy {

  urlTmp = 'http://localhost:3000/preview';
  destinationImages;
  autoShootSubject: Subject<any>;
  completamento = true;
  completamentoPollingMovement;
  perc = 0;
  radius = {};
  subs: Array<Subscription> = [];
  checkPiConnection: boolean;

  constructor(private http: HttpService, private fileManager: FileManagerService,
    private spinnerService: SpinnerLoadingService, private agisoftService: AgisoftComService,
    private memoryManager: RadiusDataService, private utilsService: UtilsService) {

    this.autoShootSubject = new Subject();
    const sub = this.autoShootSubject.subscribe((autoShootReq: AutoShotRequest) => {
        this.pollingAutoShot(autoShootReq);
    });

    this.subs.push(sub);
  }

  ngOnDestroy() {
    this.subs.map(el => el.unsubscribe());
  }

  // localhost:8080/preview          POST
  getPreview(data: PreviewRequest) {
    // AppConfig.baseUrl + 'preview'
    // const dirShoot = 'previewShootDir';
    return this.http.postRequest(AppConfig.baseUrl + 'preview', data)
      .then((result: PreviewResponse) => {
        if (result && result.photos) {
          // CREAO CARTELLA PREVIEW_DIR
          this.fileManager.createDir(PREVIEW_SHOOT_DIR, true);
          result.photos.forEach(element => {
            this.fileManager.saveImageToDiskCustom(element.name, element.data, PREVIEW_SHOOT_DIR);
          });
          this.cleanPreview();
          this.fileManager.getFileNames(PREVIEW_SHOOT_DIR);
          // console.log('postReq files path: ' + this.fileManager.previewPhotos);
          return true;
        }
      });
  }

  // localhost:8080/shootSingle    POST
  shotSingle(data: PreviewRequest) {
    // console.log('Single shot');
    // const dirShoot = 'singleShootDir';
    return this.http.postRequest(AppConfig.baseUrl + 'shootSingle', data)
      .then((result: PreviewResponse) => {
        if (result && result.photos) {
          this.fileManager.createDir(SINGLE_SHOOT_DIR, false); // --- Non cancello più in single mode
          result.photos.forEach(element => {
            this.fileManager.saveImageToDiskCustom(element.name, element.data, SINGLE_SHOOT_DIR);
          });
          this.cleanPreview();
          this.fileManager.getFileNames(SINGLE_SHOOT_DIR);
          // console.log('postReq files path: ' + this.fileManager.previewPhotos);
          return true;
        }

        return false;
      });
  }

  // localhost:8080/autoShoot         POST
  autoShootMode(autoShootReq: AutoShotRequest) {
    // Leggo dal file radius
    this.radius = this.memoryManager.readRadiusFromFile();
    autoShootReq.radius = this.radius;

    this.fileManager.createDir(AUTO_SHOOT_DIR, true);
    this.autoShoot(autoShootReq).then(r => {
      if (!r || r.esito === 'error') {
        return;
      }
      // Resetto
      this.completamento = false;
      this.perc = 0;
      /* ---------- New POLLING ----------  */
      this.autoShootSubject.next(autoShootReq);

      /* ---------- OLD POLLING  ----------  */
      /* IntervalObservable.create(1000)
        .debounceTime(300)
        .takeWhile(() => !this.completamento) // only fires when component is alive
        .subscribe(() => {
          this.pollingAutoShot(autoShootReq);
        }); */
      });
  }

  private pollingAutoShot(autoShootReq) {
    this.polling(autoShootReq).then((data: AutoShotResponse) => {
      if (data && data.esito === 'done') {
        this.spinnerService.determinateSpinner(data.perc);
        data.photos.forEach(element => {
          this.fileManager.saveImageToDiskCustom(element.name, element.data, AUTO_SHOOT_DIR);
        });
        // animazione
        setTimeout(() => {
          if (data.completamento) {
            this.spinnerService.hideSpinner();
            this.completamento = data.completamento;
          }
        }, 400);

        /* Per tornare al vecchio polling commentare l'if qua sotto */
        if (!data.completamento) {
          this.autoShootSubject.next(autoShootReq);
        }
      }
    });
  }

  private autoShoot(data: AutoShotRequest) {
    return this.http.postRequest(AppConfig.baseUrl + 'autoShoot', data)
      .then((result: AutoShotResponse) => {
        return result;
      });
  }

  private polling(data: AutoShotRequest) {
    return this.http.postRequest(AppConfig.baseUrl + 'polling', data)
      .then((result: AutoShotResponse) => {
        return result;
      });
  }

  // localhost:8080/rightMove        POST
  rightMove(planeDim, quality, continuosM?) {
    this.radius = this.memoryManager.readRadiusFromFile();
    const objReq = {
      planeDim: planeDim,
      quality: quality,
      continuousMove: continuosM, radius: this.radius };

    // console.log('Move right');
    return this.http.postRequest(AppConfig.baseUrl + 'rightMove', objReq)
      .then((result: MoveResponse) => {
        if (result) {
          this.spinnerService.indeterminateSpinner('Il carrello si sta muovendo a destra..');
          return result;
        }
      });
  }

  // localhost:8080/leftMove         POST
  leftMove(planeDim, quality, continuosM?) {
    this.radius = this.memoryManager.readRadiusFromFile();
    const objReq = {
      planeDim: planeDim,
      quality: quality,
      continuousMove: continuosM, radius: this.radius
    };

    return this.http.postRequest(AppConfig.baseUrl + 'leftMove', objReq)
      .then((result: MoveResponse) => {
        if (result) {
          this.spinnerService.indeterminateSpinner('Il carrello si sta muovendo a sinistra..');
          return result;
        }
      });
  }

  pollingFunction(repeatFunction, context) {
    this.perc = 0;
    this.completamentoPollingMovement = false;
    IntervalObservable.create(1000)
      .debounceTime(300)
      .takeWhile(() => !this.completamentoPollingMovement) // only fires when component is alive
      .subscribe(() => {
        repeatFunction.call(context);
      });
  }

  checkMovement() {
    return this.http.postRequest(AppConfig.baseUrl + 'checkMovement', {})
      .then((result: MoveResponse) => {
        if (result.esito === 'done') {
          this.completamentoPollingMovement = true;
          this.spinnerService.hideSpinner();
        }
        // console.log(result);
        return result.esito;
      });
  }

  // localhost:8080/reset            POST
  reset(planeDim) {
    const obj = { 'plane': planeDim };
    // console.log('Reset: ', obj);
    this.http.postRequest(AppConfig.baseUrl + 'reset', obj)
      .then((result: ResetResponse) => {
        // console.log(result);
      });
  }

  // localhost:8080/stopShot        POST
  stopShot() {
    this.utilsService.killProcessAgisoftByPID(this.agisoftService.pidProcessiAgisoft);
    if(this.checkPiConnection) { 
      this.stopShotRequest();
    } else {
      this.spinnerService.hideSpinner();
    }
  }

  private stopShotRequest() {
    this.http.postRequest(AppConfig.baseUrl + 'stopShoot', {})
    .then((result: StopShotResponse) => {
      this.completamento = true;
      this.spinnerService.hideSpinner();
    }).catch(err => {
      this.spinnerService.hideSpinner();
    });
  }

  // localhost:8080/restart        get --> restart delle camere
  restart() {
    this.http.getRequest(AppConfig.baseUrl + 'restart')
      .then(data => data)
      .catch(err => err);
  }

  piIsConnected() {
    return this.http.postRequest(AppConfig.baseUrl + 'piIsConnected', {})
      .then((result: Response) => {
        return result;
      }).catch(err => {
        return err;
      });
  }

  removeLoader() {
    this.completamento = true;
  }

  cleanPreview() {
    this.fileManager.previewPhotos = [];
  }

}
