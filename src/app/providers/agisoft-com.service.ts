import { Injectable, OnInit } from '@angular/core';
import { FileManagerService } from './file-manager.service';
import { PythonShell } from 'python-shell';
import { AlertNotificationService, ERROR_CODE, DONE_CODE } from './alert-notification.service';
import { SpinnerLoadingService } from './spinner-loading.service';
import { debounceTime } from 'rxjs/operators';
import { AppConfig } from '../../environments/environment.prod';
import { Subject } from 'rxjs';
import { SettingsAgisoftService } from './settings-agisoft.service';
const fixPath = require('fix-path');

export const AGISOFT_OPERATION = [
  {
    codeOp: 'PHOTO_ALIGN',
    descOp: 'Photo Alignment'
  },
  {
    codeOp: 'DENSE_CLOUD',
    descOp: 'Dense cloud'
  },
  {
    codeOp: 'MESH',
    descOp: 'Mesh'
  }, {
    codeOp: 'POLY_DECIMATION',
    descOp: 'Poly Decimation'
  },
  {
    codeOp: 'TEXTURE',
    descOp: 'Texture'
  }
];

@Injectable({
  providedIn: 'root'
})
export class AgisoftComService {

  scriptResult;
  lanciaScriptSubject: Subject<any> = new Subject();
  disableBtnAgisoft;
  pidProcessiAgisoft = [];

  constructor(private fileManger: FileManagerService, private alertNotification: AlertNotificationService,
    private spinnerService: SpinnerLoadingService, private settingsAgiService: SettingsAgisoftService) {
    fixPath();
    this.lanciaScriptSubject.pipe(debounceTime(100))
      .subscribe(requestAgisoft => {
        this.runScript(requestAgisoft)
      });
  }

  speedUpMode(quality: string, directoryImages) {
    this.generateRequestAndNotify(quality, directoryImages);
  }


  stepByStepMode(taskSelected, quality: string, directoryImages) {
    this.generateRequestAndNotify(quality, directoryImages, taskSelected);
  }

  private getTaskSelected(taskSelected) {
    let params = {};
    // Struttura che serve allo script
    if (taskSelected) {
      taskSelected.forEach(element => {
        params[element] = true;
      });
    } else {
      params = { PHOTO_ALIGN: true, DENSE_CLOUD: true, MESH: true, TEXTURE: true, POLY_DECIMATION: true };
    }

    return params;
  }

  private generateRequestAndNotify(quality: string, directoryImages, taskSelected?) {
    if (this.fileManger.isADirectory(directoryImages)) {
      let params = this.getTaskSelected(taskSelected);

      let requestAgisoft = {
        'taskSelected': params,
        'quality': quality,
        'directoryImages': directoryImages,
      };
      // this.runScript(requestAgisoft);
      requestAgisoft = this.getInfoSettings(requestAgisoft)
      if (requestAgisoft) {
        console.log('Lancio processo: ', requestAgisoft);
        this.lanciaScriptSubject.next(requestAgisoft);
      }
    }
  }

  private getInfoSettings(requestAgisoft) {
    if (this.settingsAgiService.checkDataIsRight()) {
      const requestWithSettingAgi = Object.assign({}, requestAgisoft, this.settingsAgiService.getInfoSettings());
      return requestWithSettingAgi;
    } else {
      this.alertNotification.warnMsg('Check the data insert in the settings section.  ');
    }
    return null;
  }

  toJson(objString) {
    let obj = {};
    try {
      obj = JSON.parse(objString);
    } catch (error) {
      return obj;
    }
    return obj;
  }

  showStatusAgisoft(obj: Object) {
    if (!(Object.keys(obj).length === 0 && obj.constructor === Object)) {
      const taskProgress = obj['taskProgress'];
      const taskName = obj['taskName'];
      if (taskProgress && taskName) {
        this.spinnerService.determinateSpinner(taskProgress, taskName);
      }
    }
  }

  runScript(requestAgisoft) {
    // METTERE LA CARTELLA DEGLI SCRIPT IN POSIZIONE ESTERNA AL PACHETTO BUILDATO
    const assetsDirectory = AppConfig.scriptDirectory;
    const pyshell = new PythonShell(assetsDirectory, { mode: 'text' });
    const _this = this;

    this.disableBtnAgisoft = true;
    this.spinnerService.indeterminateSpinner("Agisoft startup..", false);
    pyshell.send(JSON.stringify(requestAgisoft));
    this.pidProcessiAgisoft.push(pyshell.childProcess.pid + '');

    pyshell.on('message', function (message) {
      if (message.startsWith('*')) {
        message = message.substr(1);
      }
      // console.log(message);
      if (message.startsWith("pid_")) {
        _this.pidProcessiAgisoft.push(message.replace("pid_", ""));
      }

      const obj: any = _this.toJson(message);

      _this.showStatusAgisoft(obj);

      // Gestione dei messaggi
      _this.messageErrorOrDoneHandler(obj);
    });

    pyshell.on('error', function (error) {
       _this.messageErrorOrDoneHandler({ 'codeEsito': -2 });
    });

    pyshell.end(function (end) {
      _this.disableBtnAgisoft = false;
      _this.spinnerService.hideSpinner();
      if (_this.alertNotification.typeAlert !== ERROR_CODE) {
        _this.alertNotification.doneMsg('Creation of the project completed successfully.');
      } else {
        // console.log('TYPE ALERT END SCRIPT: ', _this.alertNotification.typeAlert)
      }

      _this.pidProcessiAgisoft = [];
      // console.log(_this.pidProcessiAgisoft);

      // setTimeout(() => {
      //   if (_this.alertNotification.typeAlert !== ERROR_CODE) {
      //     // _this.alertNotification.hideAlert();
      //   }
      // }, 2000);
    });
  }

  messageErrorOrDoneHandler(objEsito) {
    if (objEsito && objEsito.hasOwnProperty('codeEsito')) {
      if (objEsito && objEsito.codeEsito === -1) {
        const errMsg = 'Task ' + objEsito.currentOperation + ' failed. Make sure you have selected compatible operations.';
        this.alertNotification.errorMsg(errMsg);
      } if (objEsito && objEsito.codeEsito === -2) {
        // this.disableBtnAgisoft = false;
        this.alertNotification.errorMsg('Process of creating the agisoft project interrupted.');
      } else if (objEsito && objEsito.codeEsito === 0) {
        this.alertNotification.typeAlert = DONE_CODE;
      }
    }
  }

  getAgisoftOp() {
    return AGISOFT_OPERATION;
  }

}
