import { Injectable } from '@angular/core';
import * as fs from 'fs';
import * as _ from 'lodash';
import { remote } from 'electron';
import { StorageService } from './storage.service';
const storage = require('electron-json-storage');

@Injectable({
  providedIn: 'root'
})
export class RadiusDataService {
  dataDirectory = '/src/assets/data/';
  radiusPlane: any = {};

  constructor(private localStorage: StorageService) {
    this.inizializzaRadiusDefault();
  }

  private inizializzaRadiusDefault() {
    this.readRadiusFromFile();
    if (_.isEmpty(this.radiusPlane)) {
      this.saveRadius({ radius1_6: 1.6, radius3_2: 3.2 });
    }
  }

  saveRadius(objRadius) {
    this.localStorage.setData('radiusPlane', objRadius);
  }


  readRadiusFromFile() {
   this.radiusPlane = this.localStorage.getData('radiusPlane');
   return this.radiusPlane;
  }

  writeInStorage(name, data) {
    this.localStorage.setData(name, data);
  }
}
