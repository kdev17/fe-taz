import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SpinnerLoadingService {

  percentualeCompletamento: number;
  messaggio: string;
  mode: string;
  showSpinner: boolean;
  showStop;

  constructor() { }

  indeterminateSpinner(msg, showStop?) {
    this.mode = 'indeterminate';
    this.percentualeCompletamento = null;
    this.messaggio = msg;
    this.showSpinner = true;
    this.showStop = showStop === false ? false : true;
  }

  determinateSpinner(percentualeCompletamento?, msg?) {
    // msg = msg + ' \n' + percentualeCompletamento + ' %';
    this.mode = 'determinate';
    this.messaggio = msg;
    this.percentualeCompletamento = Math.floor(percentualeCompletamento);
    this.showSpinner = true;
    this.showStop = true;
  }

  isIndeterminate() {
    return this.mode === 'indeterminate';
  }

  hideSpinner() {
    this.showSpinner = false;
  }
}
