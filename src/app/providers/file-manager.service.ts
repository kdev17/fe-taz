import { Injectable } from '@angular/core';
import * as fs from 'fs';
import * as rimraf from 'rimraf';
import * as _ from 'lodash';
import { remote } from 'electron';
import { AlertNotificationService } from './alert-notification.service';
const defaultDestinationFolder = '/Users/zarfaouik/Desktop/Electron/angular-electron/src/app/providers/images';

export const PREVIEW_SHOOT_DIR = 'previewShootDir';
export const SINGLE_SHOOT_DIR = 'singleShootDir';
export const AUTO_SHOOT_DIR = 'autoShootDir';
@Injectable({
  providedIn: 'root'
})

export class FileManagerService {

  destinationImages;
  esitiSalvataggi;
  folderSelected;
  previewPhotos = [];

  constructor(private alertNotification: AlertNotificationService) {
    this.esitiSalvataggi = new Array<String>();
  }

  saveImageToDiskCustom(filename, data, folder?) {
    // this.valorizzaFolderSelected();
    const pathData = this.folderSelected + '/' + folder;
    this.decode_base64(data, pathData, filename);
  }

  createDir(folder, override) {
    const pathData = this.folderSelected + '/' + folder;
    if (fs.existsSync(pathData) && override) {
      rimraf.sync(pathData); // , function () { console.log('Done: Cartella ' + folder + ' rimossa'); }
    }
    if (!fs.existsSync(pathData)) {
      fs.mkdirSync(pathData);
    }
  }

  decode_base64(base64str, localPath, filename) {
    const buf = Buffer.from(base64str, 'base64');

    const esito = fs.writeFileSync(localPath + '/' + filename, buf);
  }

  openDirectory() {
    remote.dialog.showOpenDialog({ title: 'Select a folder', properties: ['openDirectory', 'createDirectory'] }, (folderPath) => {
      if (folderPath === undefined) {
        // console.log('You didn\'t select a folder');
        return;
      }
      this.folderSelected = folderPath[0];
      // console.log('Cartella selezionata: ' + this.folderSelected);
    });
  }

  openDirectoryReturnPath() {
    return remote.dialog.showOpenDialog({ title: 'Select a folder', properties: ['openDirectory', 'createDirectory'] });
  }



  getFileNames(specificFolder?) {

    if (!specificFolder) {
      specificFolder = PREVIEW_SHOOT_DIR;
    }
    this.previewPhotos = [];
    this.cleanCacheWindow();
    const pathPreviewImages = this.folderSelected + '/' + specificFolder;
    const namesFiles = fs.readdirSync(pathPreviewImages).reverse();
    const orderFiles = [];

    namesFiles.forEach(file => {
      if (file.endsWith('.jpg')) {
        const stats = fs.statSync(pathPreviewImages + '/' + file);
        // console.log(stats);
        orderFiles.push({
          'name': file,
          'path': 'file://' + pathPreviewImages + '/' + file,
          'ctimeMs': stats.ctimeMs
        });
      }
    });

    orderFiles.sort(function (a, b) {
      return a.ctimeMs - b.ctimeMs;
    });

    this.previewPhotos = orderFiles.slice(orderFiles.length - 12, orderFiles.length);

    return this.previewPhotos;
  }

  private cleanCacheWindow() {
    const win = remote.getCurrentWindow();
    win.webContents.session.clearCache(function () {
      // console.log('Cache della finestra pulita');
    });
  }

  isEmptyAutoDir() {
    const pathAuto = this.folderSelected + '/' + AUTO_SHOOT_DIR;
    let esito = true;
    if (!this.folderSelected) {
      this.alertNotification.warnMsg('Select the working directory.');
    } else if (!fs.existsSync(pathAuto)) {
      this.alertNotification.warnMsg('Shoot at least one time in auto mode.');
    } else {
      this.alertNotification.hideAlert();
      fs.readdirSync(pathAuto).forEach(file => {/* console.log(file) */ });
      esito = false;
    }

    return esito;
  }

  isADirectory(pathDir) {
    if (!fs.existsSync(pathDir)) {
      this.alertNotification.warnMsg('The path entered is not a folder.');
      return false;
    } else {
      this.alertNotification.hideAlert();
      return true;
    }
  }
}
