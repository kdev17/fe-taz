import { Injectable } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { FileManagerService } from './file-manager.service';
import { AlertNotificationService } from './alert-notification.service';

@Injectable({
  providedIn: 'root'
})
export class PhotoSettingService {

  photoSettingsForm;
  selectedFolder;
  msgAlert;


  constructor(private fb: FormBuilder, private fileManager: FileManagerService,
    private alertNotication: AlertNotificationService) {
    this.photoSettingsForm = fb.group({
      iso: ['', Validators.required],
      whiteBalance: ['', Validators.required],
      diafram: ['', Validators.required],
      shutterSpeed: ['', Validators.required],
    });
  }

  formIsValid() {
    if (this.photoSettingsForm.invalid) {
      this.alertNotication.warnMsg('Insert all the parameters.');
      // this.msgAlert = 'Inserire tutti i parametri.';
    } else if (!this.fileManager.folderSelected) {
      this.alertNotication.warnMsg('Select destination folder.');
      // this.msgAlert = 'Selezionare la cartella di destinazione.';
    } else {
      this.alertNotication.hideAlert();
      // this.msgAlert = null;
      return true;
    }
  }

  autoCompleteForm() {
    // this.photoSettingsForm.values = { 'iso': '800', 'whiteBalance': 'Daylight', 'diafram': '4.5', 'shutterSpeed': '30' };
    this.photoSettingsForm = this.fb.group({
      iso: ['800', Validators.required],
      whiteBalance: ['Daylight', Validators.required],
      diafram: ['4.5', Validators.required],
      shutterSpeed: ['30', Validators.required],
    });
  }
}
