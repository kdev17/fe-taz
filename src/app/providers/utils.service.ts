import { Injectable } from '@angular/core';
const ps = require('ps-node');
const shellExec = require('shell-exec')

@Injectable()
export class UtilsService {

  constructor() { }

  killProcessAgisoftByPID(pidArray: Array<any>) {
    console.log('TERMINO I SEGUENTI PROCESSI ASSOCIATI AD AGISOFT: ', pidArray,pidArray.length)
    pidArray.forEach(pid => {
      if (navigator.platform.toLowerCase().indexOf("win") >= 0)
        // windows command
        var commandShell = 'taskkill /F /PID ' + pid
      else
        // linux command
        var commandShell = 'kill -2 ' + pid;
      
      shellExec(commandShell).then(console.log)
                             .catch(console.log);
    });

    return [];
  }
}
