import { TestBed, inject } from '@angular/core/testing';

import { RadiusDataService } from './save-data.service';

describe('RadiusDataService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RadiusDataService]
    });
  });

  it('should be created', inject([RadiusDataService], (service: RadiusDataService) => {
    expect(service).toBeTruthy();
  }));
});
