import { Injectable } from '@angular/core';

export const ERROR_CODE = '-1';
export const DONE_CODE = '0';
export const WARN_CODE = '1';

@Injectable({
  providedIn: 'root'
})
export class AlertNotificationService {

  messaggio;
  typeAlert;
  showAlert;

  constructor() { }

  errorMsg(msg) {
    this.messaggio = msg;
    this.typeAlert = ERROR_CODE;
    this.showAlert = true;
  }

  doneMsg(msg) {
    this.messaggio = msg;
    this.typeAlert = DONE_CODE;
    this.showAlert = true;
  }

  warnMsg(msg) {
    this.messaggio = msg;
    this.typeAlert = WARN_CODE;
    this.showAlert = true;
  }

  hideAlert() {
    // this.showAlert = false;
  }

  hideAlertForce() {
    this.showAlert = false;
  }

}
