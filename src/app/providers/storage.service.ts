import { Injectable } from '@angular/core';
const Store = require('electron-store');
const store = new Store();

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor() { }

  setData(name, data) {
    store.set(name, data);
  }

  getData(name) {
    return store.get(name);
  }

}
