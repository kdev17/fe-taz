import { TestBed, inject } from '@angular/core/testing';

import { SpinnerLoadingService } from './spinner-loading.service';

describe('SpinnerLoadingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SpinnerLoadingService]
    });
  });

  it('should be created', inject([SpinnerLoadingService], (service: SpinnerLoadingService) => {
    expect(service).toBeTruthy();
  }));
});
