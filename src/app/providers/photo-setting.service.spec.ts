import { TestBed, inject } from '@angular/core/testing';

import { PhotoSettingService } from './photo-setting.service';

describe('PhotoSettingService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PhotoSettingService]
    });
  });

  it('should be created', inject([PhotoSettingService], (service: PhotoSettingService) => {
    expect(service).toBeTruthy();
  }));
});
