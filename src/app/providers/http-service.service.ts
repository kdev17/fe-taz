import { Injectable } from '@angular/core';
import { AlertNotificationService } from './alert-notification.service';
import { SpinnerLoadingService } from './spinner-loading.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  axios = require('axios');

  constructor(private alertNotification: AlertNotificationService,
    private spinnerService: SpinnerLoadingService) { }

  getRequest(url) {
    return this.callRequest('GET', url);
  }

  postRequest(url, data) {
    return this.callRequest('POST', url, data);
  }

  callRequest(method, url, data?) {
    return this.axios({
      method: method,
      url: url,
      data: data,
      headers: {
      'Access-Control-Allow-Origin': '*',
    }

    }).then(response => this.getData(response))
      .catch(error => this.checkErrors(error));
  }

  // Controlla status request
  checkErrors(error) {
    if (error.response) {
      const status = error.response.status;
      if (status !== 200) {
        const statusText = error.response.statusText;
        const reqUrl = error.request.responseURL;
        this.alertNotification.errorMsg(status + ' - ' + statusText + ' | URL -->' + reqUrl);
      }
    } else {
      this.alertNotification.errorMsg(error.message + ': check if the server is active.');
    }
    // this.spinnerService.hideSpinner();
  }

  // Estrapola i dati e controlla esito interno
  getData(response) {
    const data = response.data;
    if (data.esito === 'error') {
      this.alertNotification.errorMsg('Communication error with the server.');
    }
    return data;
  }
}
