import { TestBed } from '@angular/core/testing';

import { SettingsAgisoftService } from './settings-agisoft.service';

describe('SettingsAgisoftService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SettingsAgisoftService = TestBed.get(SettingsAgisoftService);
    expect(service).toBeTruthy();
  });
});
