import { Injectable } from '@angular/core';
import * as _ from 'lodash';

@Injectable({
  providedIn: 'root'
})
export class SettingsAgisoftService {

  tiePointLimit = 4000;
  keyPointLimit = 40000;
  decimateModelPercentage = 15;

  constructor() { }


  getInfoSettings() {
    const obj = {
      'tie_point': this.keyPointLimit,
      'key_point': this.tiePointLimit,
      'decimate_model_perc': this.decimateModelPercentage
    }
    Object.keys(obj).forEach((key) => (obj[key] == null || obj[key] == undefined) && delete obj[key]);

    return obj;
  }

  checkDataIsRight() {
    const condKeyPoint = isNaN(this.keyPointLimit) || (!isNaN(this.keyPointLimit) && this.keyPointLimit >= 0);
    const condTiePoint = isNaN(this.tiePointLimit) || (!isNaN(this.tiePointLimit) && this.tiePointLimit >= 0);
    const condDecimatePerc = isNaN(this.decimateModelPercentage) || (!isNaN(this.decimateModelPercentage) && 
                                                                      this.decimateModelPercentage >= 0 && 
                                                                      this.decimateModelPercentage <= 100);

    return condDecimatePerc && condKeyPoint && condTiePoint;
  }
}
