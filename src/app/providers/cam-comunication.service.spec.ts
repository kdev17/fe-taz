import { TestBed, inject } from '@angular/core/testing';

import { CamComunicationService } from './cam-comunication.service';

describe('CamComunicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CamComunicationService]
    });
  });

  it('should be created', inject([CamComunicationService], (service: CamComunicationService) => {
    expect(service).toBeTruthy();
  }));
});
